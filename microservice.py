from app import create_app, db
from app.api.models import Currency, Quotation

app = create_app()

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Currency': Currency, 'Quotation': Quotation}
