"""empty message

Revision ID: 0d0e0d3a8783
Revises: 
Create Date: 2019-03-22 22:18:27.306194

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0d0e0d3a8783'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('currency',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=64), nullable=False),
    sa.Column('initials', sa.String(length=3), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_currency_initials'), 'currency', ['initials'], unique=True)
    op.create_index(op.f('ix_currency_name'), 'currency', ['name'], unique=True)
    op.create_table('quotation',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('timestamp', sa.DateTime(), nullable=False),
    sa.Column('value', sa.Float(), nullable=False),
    sa.Column('variation', sa.Float(), nullable=False),
    sa.Column('currency_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['currency_id'], ['currency.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('quotation')
    op.drop_index(op.f('ix_currency_name'), table_name='currency')
    op.drop_index(op.f('ix_currency_initials'), table_name='currency')
    op.drop_table('currency')
    # ### end Alembic commands ###
