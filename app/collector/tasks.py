from app import scheduler
from app.collector.hgcollector import get_quotations_from_hg, insert_quotation


@scheduler.task('interval', minutes=30)
def scheduled_task():
    with scheduler.app.app_context():
        currencies = get_quotations_from_hg()
        for currency in currencies:
            insert_quotation(currencies[currency], currency)
        print('Task Alive')
