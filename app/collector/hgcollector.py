import requests
import json

from app import db
from app.api.models import Quotation, Currency


def get_quotations_from_hg():
    hg__api_url = 'https://api.hgbrasil.com/finance/quotations?key=1f27bc69'
    response = requests.get(hg__api_url)

    if response.status_code == 200:
        j_response = json.loads(response.content.decode('utf-8'))
        results = j_response['results']['currencies']

        if 'source' in results:
            del results['source']

        return results
    else:
        return None


def insert_quotation(d_quotation, currency_initials):
    new_quotation = Quotation()
    currency = Currency.query.filter_by(initials=currency_initials).first()
    if currency is not None:
        new_quotation.currency = currency
        new_quotation.value = d_quotation['buy']
        new_quotation.variation = d_quotation['variation']
        db.session.add(new_quotation)
        db.session.commit()


def get_all_currencies():
    return Currency.query.all()
