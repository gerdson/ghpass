from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_apscheduler import APScheduler
from flask_cors import CORS


db = SQLAlchemy()
migrate = Migrate()
scheduler = APScheduler()

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)
    scheduler.init_app(app)
    scheduler.start()

    cors = CORS(app, resources={r"/*": {"origins": "*"}})


    from app.api import bp as api_bp
    app.register_blueprint(api_bp)

    from app.collector import bp as collector_bp
    app.register_blueprint(collector_bp, url_prefix='/collector')

    @app.before_first_request
    def load_tasks():
        from app.collector import tasks

    return app
