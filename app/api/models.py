from flask import url_for

from app import db
from datetime import datetime

class Currency(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True, nullable=False)
    initials = db.Column(db.String(3), index=True, unique=True, nullable=False)

    def __repr__(self):
        return '<Currency {}>'.format(self.name)

    def to_dict(self):
        data = {
            'id': self.id,
            'name': self.name,
            'initials': self.initials,
            '_links': {
                'self': url_for('api.get_currency', id=self.id)
            }
        }

        return data

    def from_dict(self, data):
        for field in ['name', 'initials']:
            if field in data:
                setattr(self, field, data[field])


class Quotation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())
    value = db.Column(db.Float, nullable=False)
    variation = db.Column(db.Float, nullable=False)

    currency_id = db.Column(db.Integer, db.ForeignKey('currency.id'), nullable=False)
    currency = db.relationship('Currency', backref=db.backref('variations', lazy=True))

    def __repr__(self):
        return '<Quotation {}>'.format(self.id)

    def to_dict(self):
        data = {
            'id': self.id,
            'timestamp': self.timestamp,
            'value': self.value,
            'variation': self.variation,
            'currency_id': self.currency_id
        }

        return data


    def from_dict(self, data):
        for field in ['timestamp', 'values', 'currency_id']:
            if field in data:
                setattr(self, field, data[field])
