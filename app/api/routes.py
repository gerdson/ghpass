import json
from flask import jsonify

from app import db
from app.api import bp
from app.api.models import Currency, Quotation


@bp.route('/')
@bp.route('/currencies', methods=['GET'])
def get_currencies():
    currencies = Currency.query.all()
    data = {
        'currencies': []
    }

    for currency in currencies:
        data['currencies'].append(currency.to_dict())

    return jsonify(data)


@bp.route('/currencies/<int:id>', methods=['GET'])
def get_currency(id):
    return jsonify(Currency.query.get_or_404(id).to_dict())


@bp.route('/currencies/<int:currency_id>/quotations')
def get_currency_quotations(currency_id):
    result = {}
    currency = Currency.query.get_or_404(currency_id)
    result['currency'] = currency.to_dict()
    result['quotations'] = []

    quotations = Quotation.query.order_by(Quotation.id.desc()).filter_by(currency=currency).limit(40)
    for quotation in quotations:
        result['quotations'].append(quotation.to_dict())
    return jsonify(result)

@bp.route('/last-quotations', methods=['GET'])
def get_last_quotations():
    currencies = Currency.query.all()
    result = {'results': []}

    for currency in currencies:
        d_currency = {'currency': currency.to_dict()}
        quotation = Quotation.query.filter_by(currency=currency).order_by(Quotation.id.desc()).limit(1)
        d_currency['quotation'] = quotation.first().to_dict()

        result['results'].append(d_currency)

    return jsonify(result)

@bp.route('/quotations', methods=['GET'])
def get_quotations():
    currencies = Currency.query.all()
    result = {'results': []}

    for currency in currencies:
        d_currency = {'currency': currency.to_dict(), 'quotations': []}
        quotations = Quotation.query.order_by(Quotation.id.desc()).filter_by(currency=currency).limit(40)
        for quotation in quotations:
            d_currency['quotations'].append(quotation.to_dict())
        result['results'].append(d_currency)

    return jsonify(result)


@bp.route('/initial-data', methods=['GET'])
def insert_initial_data():
    jfile = open('app/api/initial_data.json', 'r')
    initial_data = json.load(jfile)

    for currency in initial_data['currencies']:
        item = Currency.query.filter_by(initials=currency['initials']).first()
        if item is None:
            new_currency = Currency(initials=currency['initials'], name=currency['name'])
            db.session.add(new_currency)

    db.session.commit()

    currencies = Currency.query.all()
    data = {
        'currencies': []
    }

    for currency in currencies:
        data['currencies'].append(currency.to_dict())

    return jsonify(data)





